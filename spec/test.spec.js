import {verifyProof} from '../index.js'
import { testCases } from './merkle-tree-testcases'
describe('verify proof', () => {
  it('should return true if the proof is valid', async function() {
    let verification = verifyProof(
      testCases[0].email,
      testCases[0].public_key,
      testCases[0].merkle_root,
      testCases[0].neighbors
    )
    expect(await verification).toEqual(true)

    let verification1 = verifyProof(
      testCases[1].email,
      testCases[1].public_key,
      testCases[1].merkle_root,
      testCases[1].neighbors
    )
    expect(await verification1).toBe(true)
  
    let verification2 =  verifyProof(
      testCases[2].email,
      testCases[2].public_key,
      testCases[2].merkle_root,
      testCases[2].neighbors
    )
    expect(await verification2).toBe(true)
  
  
  let verification3 =  verifyProof(
    testCases[3].email,
    testCases[3].public_key,
    testCases[3].merkle_root,
    testCases[3].neighbors
  )
    expect(await verification3).toBe(true)

  let verification4 =  verifyProof(
    testCases[4].email,
    testCases[4].public_key,
    testCases[4].merkle_root,
    testCases[4].neighbors
  )
    expect(await verification4).toBe(true)
  let verification5 =  verifyProof(
    testCases[5].email,
    testCases[5].public_key,
    testCases[5].merkle_root,
    testCases[5].neighbors
  )
    expect(await verification5).toBe(true)
  let verification6 =  verifyProof(
    testCases[6].email,
    testCases[6].public_key,
    testCases[6].merkle_root,
    testCases[6].neighbors
  )
    expect(await verification6).toBe(true)
  let verification7 =  verifyProof(
    testCases[7].email,
    testCases[7].public_key,
    testCases[7].merkle_root,
    testCases[7].neighbors
  )
    expect(await verification7).toBe(true)
  });

  it('should return false if the proof is invalid', async function() {  
    let verification8 =  verifyProof(
      testCases[4].email,
      testCases[8].public_key,
      testCases[8].merkle_root,
      testCases[8].neighbors
    )
      expect(await verification8).toBe(false)
    let verification14 =  verifyProof(
      testCases[14].email,
      testCases[15].public_key,
      testCases[15].merkle_root,
      testCases[15].neighbors
    )
      expect(await verification14).toBe(false)
    let verification9 =  verifyProof(
      testCases[15].email,
      testCases[14].public_key,
      testCases[15].merkle_root,
      testCases[15].neighbors
    )
      expect(await verification9).toBe(false)
    let verification10 =  verifyProof(
      testCases[14].email,
      testCases[15].public_key,
      testCases[15].merkle_root,
      testCases[15].neighbors
    )
      expect(await verification10).toBe(false)
    let verification11 =  verifyProof(
      testCases[14].email,
      testCases[15].public_key,
      testCases[15].merkle_root,
      testCases[15].neighbors
    )
      expect(await verification11).toBe(false)
    let verification12 =  verifyProof(
      testCases[11].email,
      testCases[14].public_key,
      testCases[14].merkle_root,
      testCases[14].neighbors
    )
      expect(await verification12).toBe(false)
    let verification13 =  verifyProof(
      testCases[15].email,
      testCases[15].public_key,
      testCases[5].merkle_root,
      testCases[15].neighbors
    )
      expect(await verification13).toBe(false)
  });
});


