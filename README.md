pk_verify
==========

## A client library for preforming verification of CONIKS proofs

pk_verify is a verification tool to enable better use of end-to-end encryption services.  Its aim is to reduce the need to trust an intermediary to serve valid copies of the users' public key. Specifically this library is an implementation of the [CONIKS](https://eprint.iacr.org/2014/1004.pdf) protocol. It's meant to be included in client side software to verify proofs provided by an Identity Provider (See §4.1.4 of the CONIKS paper). A valid proof means that the public key is a member of the signed tree root provided (STR). Client software implemented using this library should perform validation of the user's own public keys in the Identity Provider's (IP) directory with each epoch (as defined by the IP). This process is known as 'monitoring'. It should also ensure that the STRs provided are consistent with previous STRs and those being provided elsewhere through a process called 'auditing'.  Monitoring is within the scope of this library while auditing is not.

## Security Warning
This library is a work in progress and shouldn't be used in production environments.

## Usage
```javascript
import verifyProof from 'pk_verify';

verifyProof(email, public_key, merkle_root, neighbors).then(
    (valid) => {
        valid ? //if true encrypt message and send
        : //if false show the user a huge honking flashing error message
    }
    
).catch(e => console.error(e))
```
## Future work
Facilitate the monitoring protocol by providing a functions which:
 - request proofs for user's keys from  an Identity Provider 
 - enforce strict key change policies automatically
 - track epochs, and 'catch up'

Facilitate auditing by :
 - enabling users to define their Identity Providers and 
 - enabling users to perform the same lookup on multiple Identity providers with one function call
 - provide tools for comparing STRs 
 - provide tools for ensuring that an STR is part of an unbroken hash chain

 Facilitate whistleblowing ?